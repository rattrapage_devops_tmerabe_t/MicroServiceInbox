<?php
Doo::loadCore('db/DooModel');

class VillesBase extends DooModel{

    /**
     * @var mediumint Max length is 8.  unsigned.
     */
    public $ville_id;

    /**
     * @var varchar Max length is 3.
     */
    public $ville_departement;

    /**
     * @var varchar Max length is 255.
     */
    public $ville_slug;

    /**
     * @var varchar Max length is 45.
     */
    public $ville_nom;

    /**
     * @var varchar Max length is 45.
     */
    public $ville_nom_simple;

    /**
     * @var varchar Max length is 45.
     */
    public $ville_nom_reel;

    /**
     * @var varchar Max length is 20.
     */
    public $ville_nom_soundex;

    /**
     * @var varchar Max length is 22.
     */
    public $ville_nom_metaphone;

    /**
     * @var varchar Max length is 255.
     */
    public $ville_code_postal;

    /**
     * @var varchar Max length is 3.
     */
    public $ville_commune;

    /**
     * @var varchar Max length is 5.
     */
    public $ville_code_commune;

    /**
     * @var smallint Max length is 3.  unsigned.
     */
    public $ville_arrondissement;

    /**
     * @var varchar Max length is 4.
     */
    public $ville_canton;

    /**
     * @var smallint Max length is 5.  unsigned.
     */
    public $ville_amdi;

    /**
     * @var mediumint Max length is 11.  unsigned.
     */
    public $ville_population_2010;

    /**
     * @var mediumint Max length is 11.  unsigned.
     */
    public $ville_population_1999;

    /**
     * @var mediumint Max length is 10.  unsigned.
     */
    public $ville_population_2012;

    /**
     * @var int Max length is 11.
     */
    public $ville_densite_2010;

    /**
     * @var float
     */
    public $ville_surface;

    /**
     * @var float
     */
    public $ville_longitude_deg;

    /**
     * @var float
     */
    public $ville_latitude_deg;

    /**
     * @var varchar Max length is 9.
     */
    public $ville_longitude_grd;

    /**
     * @var varchar Max length is 8.
     */
    public $ville_latitude_grd;

    /**
     * @var varchar Max length is 9.
     */
    public $ville_longitude_dms;

    /**
     * @var varchar Max length is 8.
     */
    public $ville_latitude_dms;

    /**
     * @var mediumint Max length is 4.
     */
    public $ville_zmin;

    /**
     * @var mediumint Max length is 4.
     */
    public $ville_zmax;

    public $_table = 'villes';
    public $_primarykey = 'ville_id';
    public $_fields = array('ville_id','ville_departement','ville_slug','ville_nom','ville_nom_simple','ville_nom_reel','ville_nom_soundex','ville_nom_metaphone','ville_code_postal','ville_commune','ville_code_commune','ville_arrondissement','ville_canton','ville_amdi','ville_population_2010','ville_population_1999','ville_population_2012','ville_densite_2010','ville_surface','ville_longitude_deg','ville_latitude_deg','ville_longitude_grd','ville_latitude_grd','ville_longitude_dms','ville_latitude_dms','ville_zmin','ville_zmax');

    public function getVRules() {
        return array(
                'ville_id' => array(
                        array( 'integer' ),
                        array( 'min', 0 ),
                        array( 'maxlength', 8 ),
                        array( 'optional' ),
                ),

                'ville_departement' => array(
                        array( 'maxlength', 3 ),
                        array( 'optional' ),
                ),

                'ville_slug' => array(
                        array( 'maxlength', 255 ),
                        array( 'optional' ),
                ),

                'ville_nom' => array(
                        array( 'maxlength', 45 ),
                        array( 'optional' ),
                ),

                'ville_nom_simple' => array(
                        array( 'maxlength', 45 ),
                        array( 'optional' ),
                ),

                'ville_nom_reel' => array(
                        array( 'maxlength', 45 ),
                        array( 'optional' ),
                ),

                'ville_nom_soundex' => array(
                        array( 'maxlength', 20 ),
                        array( 'optional' ),
                ),

                'ville_nom_metaphone' => array(
                        array( 'maxlength', 22 ),
                        array( 'optional' ),
                ),

                'ville_code_postal' => array(
                        array( 'maxlength', 255 ),
                        array( 'optional' ),
                ),

                'ville_commune' => array(
                        array( 'maxlength', 3 ),
                        array( 'optional' ),
                ),

                'ville_code_commune' => array(
                        array( 'maxlength', 5 ),
                        array( 'notnull' ),
                ),

                'ville_arrondissement' => array(
                        array( 'integer' ),
                        array( 'min', 0 ),
                        array( 'maxlength', 3 ),
                        array( 'optional' ),
                ),

                'ville_canton' => array(
                        array( 'maxlength', 4 ),
                        array( 'optional' ),
                ),

                'ville_amdi' => array(
                        array( 'integer' ),
                        array( 'min', 0 ),
                        array( 'maxlength', 5 ),
                        array( 'optional' ),
                ),

                'ville_population_2010' => array(
                        array( 'integer' ),
                        array( 'min', 0 ),
                        array( 'maxlength', 11 ),
                        array( 'optional' ),
                ),

                'ville_population_1999' => array(
                        array( 'integer' ),
                        array( 'min', 0 ),
                        array( 'maxlength', 11 ),
                        array( 'optional' ),
                ),

                'ville_population_2012' => array(
                        array( 'integer' ),
                        array( 'min', 0 ),
                        array( 'maxlength', 10 ),
                        array( 'optional' ),
                ),

                'ville_densite_2010' => array(
                        array( 'integer' ),
                        array( 'maxlength', 11 ),
                        array( 'optional' ),
                ),

                'ville_surface' => array(
                        array( 'float' ),
                        array( 'optional' ),
                ),

                'ville_longitude_deg' => array(
                        array( 'float' ),
                        array( 'optional' ),
                ),

                'ville_latitude_deg' => array(
                        array( 'float' ),
                        array( 'optional' ),
                ),

                'ville_longitude_grd' => array(
                        array( 'maxlength', 9 ),
                        array( 'optional' ),
                ),

                'ville_latitude_grd' => array(
                        array( 'maxlength', 8 ),
                        array( 'optional' ),
                ),

                'ville_longitude_dms' => array(
                        array( 'maxlength', 9 ),
                        array( 'optional' ),
                ),

                'ville_latitude_dms' => array(
                        array( 'maxlength', 8 ),
                        array( 'optional' ),
                ),

                'ville_zmin' => array(
                        array( 'integer' ),
                        array( 'maxlength', 4 ),
                        array( 'optional' ),
                ),

                'ville_zmax' => array(
                        array( 'integer' ),
                        array( 'maxlength', 4 ),
                        array( 'optional' ),
                )
            );
    }

}