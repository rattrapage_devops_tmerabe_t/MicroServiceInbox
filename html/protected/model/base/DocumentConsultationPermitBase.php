<?php
Doo::loadCore('db/DooModel');

class DocumentConsultationPermitBase extends DooModel{

    /**
     * @var int Max length is 11.
     */
    public $idDocumentConsultationPermit;

    /**
     * @var int Max length is 11.
     */
    public $idUser;

    /**
     * @var int Max length is 11.
     */
    public $idAnnonce;

    public $_table = 'document_consultation_permit';
    public $_primarykey = 'idDocumentConsultationPermit';
    public $_fields = array('idDocumentConsultationPermit','idUser','idAnnonce');

    public function getVRules() {
        return array(
                'idDocumentConsultationPermit' => array(
                        array( 'integer' ),
                        array( 'maxlength', 11 ),
                        array( 'optional' ),
                ),

                'idUser' => array(
                        array( 'integer' ),
                        array( 'maxlength', 11 ),
                        array( 'notnull' ),
                ),

                'idAnnonce' => array(
                        array( 'integer' ),
                        array( 'maxlength', 11 ),
                        array( 'notnull' ),
                )
            );
    }

}