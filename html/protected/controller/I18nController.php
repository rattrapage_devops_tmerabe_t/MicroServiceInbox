<?php
Doo::LoadModel('Language');

/**
 * Internationalization Controller
 * Controller servant à la gestion des langues du projet
 * @author darkredz
 * @package Controller
 */
class I18nController extends DooController{
    var $langs = array('fr', 'en');

    function  __construct() {
        //Site non multilingue
        Doo::conf()->lang = 'fr';
        return;
    }

    public static function _getCurrentLanguageId()
    {
        $allLanguages = Language::_getAlllanguages();
        foreach ($allLanguages as $key) {
            if (Doo::conf()->lang == $key['code']) {
                return $key['idLanguage'];
            }
        }

        return Doo::conf()->DEFAULT_LANG['idLanguage'];
    }

}
