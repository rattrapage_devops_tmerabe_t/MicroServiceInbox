-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Client :  127.0.0.1
-- Généré le :  Sam 26 Août 2017 à 17:22
-- Version du serveur :  5.7.14
-- Version de PHP :  5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `crush`
--

-- --------------------------------------------------------

--
-- Structure de la table `inbox`
--

CREATE TABLE `inbox` (
  `id` int(50) NOT NULL,
  `from` int(50) NOT NULL,
  `to` int(50) NOT NULL,
  `match_id` int(50) NOT NULL,
  `sent_date` date NOT NULL,
  `created_date` date NOT NULL,
  `content` varchar(1000) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Contenu de la table `inbox`
--

INSERT INTO `inbox` (`id`, `from`, `to`, `match_id`, `sent_date`, `created_date`, `content`) VALUES
(1, 1, 2, 12, '2017-08-01', '2017-08-02', 'coucou mdr'),
(2, 2, 1, 12, '2017-08-01', '2017-08-17', 'coucou ptdr'),
(3, 3, 2, 32, '2017-01-01', '2017-01-01', 'wesh'),
(14, 3, 2, 32, '2017-01-01', '2017-01-01', 'lol'),
(87, 3, 2, 32, '2017-01-01', '2017-01-01', 'lol');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
