<?php
Doo::loadModel('Region');
Doo::loadModel('Departement');
Doo::loadModel('Villes');
Doo::loadController('BDDController');

class RegionVilleDepartementCtrl extends BDDController {
	public function getAllRegion() {
        $r = new Region();
        $regions = $r->find(array('select' => '*', 'AsArray' => 'true'));
		return $this->renderJSON(json_encode($regions));
	}
	
	public function getDepartementByRegion() {
        $r = new Departement();
        $departement = $r->find(array('select' => '*', 'AsArray' => 'true', 'where' => 'region_code = ?', 'param' => array($this->params['region_code'])));
		return $this->renderJSON(json_encode($departement));
	}
	
	public function getVilleByDepartement() {
        $r = new Villes();
        $ville = $r->find(array('select' => '*', 'AsArray' => 'true', 'where' => 'ville_departement = ?', 'param' => array($this->params['ville_departement'])));
		return $this->renderJSON(json_encode($ville));
	}
}