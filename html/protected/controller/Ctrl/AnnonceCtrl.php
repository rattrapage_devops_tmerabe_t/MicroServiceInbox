<?php
Doo::loadModel('UserAnnonceRelation');
Doo::loadModel('DocumentConsultationPermit');
Doo::loadModel('Users');
Doo::loadModel('Annonces');
Doo::loadModel('Photos');
Doo::loadModel('PhotoAnnonce');
Doo::loadController('BDDController');

class AnnonceCtrl extends BDDController {

	public function favorisByIdUser() {

		$a = new Annonces();
		$opt = array(
				'where' => 'users.idUser = ?',
				'param' => array(
						$this->params['idUser']
				)
		);
		isset($this->params['idUser']) ? $annonces = $a->relate("Annonces", "Users", $opt, $this->params['idUser']) : $annonces = $a->relate("Annonces", "Users", $opt);
		
		return $this->renderJSON(json_encode($annonces));
	}

	public function annoncesByLatLng() {

		$data = json_decode(file_get_contents("php://input"));
		$sqlLatLng = " ";
		$len = sizeof($data);
		for ($i = 0; $i < $len; $i++) {
			$sqlLatLng .= " (lat=" . $data[$i]->lat . " AND lng=" . $data[$i]->lng . ") OR";
		}
		$sqlLatLng = substr($sqlLatLng, 0, strlen($sqlLatLng) - 3);
		
		$a = new Annonces();
		$opt = array(
				'where' => $sqlLatLng
		);
		isset($this->params['idUser']) ? $annonces = $a->find($opt, $this->params['idUser']) : $annonces = $a->find($opt);
		return $this->renderJSON(json_encode($annonces));
	}

	public function getOneById() {

		$a = new Annonces();
		$opt = array(
				'where' => 'annonces.idAnnonce = ?',
				'param' => array(
						$this->params['idAnnonce']
				)
		);
		isset($this->params['idUser']) ? $annonces = $a->find($opt, $this->params['idUser']) : $annonces = $a->find($opt);
		if (empty($annonces)) {
			return $this->renderJSON(json_encode($annonces));
		}
		return $this->renderJSON(json_encode(array_pop($annonces)));
	}

	public function getAnnoncesByCity() {

		$a = new Annonces();
		$opt = array(
				'where' => 'ville = ?',
				'param' => array(
						$this->params['ville']
				)
		);
		isset($this->params['idUser']) ? $annonces = $a->find($opt, $this->params['idUser']) : $annonces = $a->find($opt);
		return $this->renderJSON(json_encode($annonces));
	}

	public function getAllAnnonces() {

		$a = new Annonces();
		isset($this->params['idUser']) ? $annonces = $a->find(null, $this->params['idUser']) : $annonces = $a->find();
		return $this->renderJSON(json_encode($annonces));
	}

	public function possessionByIdUser() {

		$a = new Annonces();
		$opt = array(
				'where' => 'idProprio = ?',
				'param' => array(
						$this->params['idUser']
				)
		);
		isset($this->params['idUser']) ? $annonces = $a->find($opt, $this->params['idUser']) : $annonces = $a->find($opt);
		return $this->renderJSON(json_encode($annonces));
	}

	public function saveDocumentConsultationPermit() {

		$data = json_decode(file_get_contents("php://input"));
		$dcp = new DocumentConsultationPermit($data);
		return $this->renderJSON(json_encode($dcp->insert()));
	}

	public function delDocumentConsultationPermit() {

		$del = DocumentConsultationPermit::_delete('DocumentConsultationPermit', array(
				'where' => 'idAnnonce = ? AND idUser = ?',
				'param' => array(
						$this->params['idAnnonce'],
						$this->params['idUser']
				)
		));
		return $this->renderJSON(json_encode($del));
	}

	public function saveFavorisAnnonce() {

		$data = json_decode(file_get_contents("php://input"));
		$ua = new UserAnnonceRelation($data);
		return $this->renderJSON(json_encode($ua->insert()));
	}

	public function delFavorisAnnonce() {

		$del = UserAnnonceRelation::_delete('UserAnnonceRelation', array(
				'where' => 'idAnnonce = ? AND idUser = ?',
				'param' => array(
						$this->params['idAnnonce'],
						$this->params['idUser']
				)
		));
		return $this->renderJSON(json_encode($del));
	}

	public function savePossessionAnnonce() {

		$data = json_decode(file_get_contents("php://input"));
		$a = new Annonces($data);
		if (! $a->idAnnonce) {
			$a->idProprio = $this->params['idUser'];
			return $this->renderJSON(json_encode($a->insert()));
		}
		return $this->renderJSON(json_encode($a->update()));
	}

	public function deleteAnnonce() {

		$del = Annonces::_delete('Annonces', array(
				'where' => 'idAnnonce = ?',
				'param' => array(
						$this->params['idAnnonce']
				)
		));
		return $this->renderJSON(json_encode($del));
	}

}