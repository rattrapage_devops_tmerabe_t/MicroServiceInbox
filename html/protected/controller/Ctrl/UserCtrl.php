<?php
Doo::loadModel('Users');
Doo::loadController('BDDController');

class UserCtrl extends BDDController {

	public function getOneById() {

		$u = new Users();
		$options = array(
				'select' => '*',
				'AsArray' => 'true',
				'where' => "id = '" . $this->params['idUser'] . "'"
		);
		$users = $u->find($options);

		unset($users[0]->_table);
		unset($users[0]->_primarykey);
		unset($users[0]->_fields);	

		if (empty($users)) {
			return $this->renderJSON(json_encode(null));
		}
		return $this->renderJSON(json_encode(array_pop($users)));
	}

	public function saveOne() {
		$data = file_get_contents("php://input");
		$data = json_decode($data);
		// if ($data->id) {
		// 	$u = new Users($data);
		// 	return $this->renderJSON(json_encode($u->update()));
		// }
		$u = new Users();
		$options = array(
				'select' => '*',
				'AsArray' => 'true',
				'where' => "email = '" . $data->email . "'"
		);
		$users = $u->find($options);
		if (empty($users)) {
			$newUser = new Users($data);
			return $this->renderJSON(json_encode($newUser->insert()));
		}
		return $this->renderJSON(json_encode("Cet email existe déjà."), self::ERR_DEV);
	}

		public function UpdateOne() {

		$data = file_get_contents("php://input");
		$data = json_decode($data);


		if ($data->id) {
			$u = new Users($data);
			return $this->renderJSON(json_encode($u->update()));
		}
		}
	


	public function getAll() {

		$u = new Users();
		$options = array(
				'select' => '*',
				'AsArray' => 'true'
		);
		$users = $u->find($options);
		foreach ($users as $u) {
			unset($u->_table);
			unset($u->_primarykey);
			unset($u->_fields);		
		}
		return $this->renderJSON(json_encode($users));
	}

	public function connectUser() {

		$email = $this->params['email'];
		$u = new Users();
		$options = array(
				'select' => '*',
				'AsArray' => 'true',
				'where' => "email = '$email'"
		);
		$users = $u->find($options);
		if (empty($users)) {
			return $this->renderJSON(json_encode("Email incorrect"), self::ERR_DEV);
		} else if (! password_verify($this->params['pwd'], $users[0]->motDePasse)) {
			return $this->renderJSON(json_encode("Mot de passe incorrect"), self::ERR_DEV);
		}
		return $this->renderJSON(json_encode(array_pop($users)));
	}

	public function getLocataireUsers() {

		$u = new Users();
		$options = array(
				'select' => '*',
				'AsArray' => 'true',
				'where' => "typeProfil = 'loc'"
		);
		$users = $u->find($options);
		if (empty($users)) {
			return $this->renderJSON(json_encode($this->db->error), self::ERR_SYS);
		}
		return $this->renderJSON(json_encode($users));
	}

}