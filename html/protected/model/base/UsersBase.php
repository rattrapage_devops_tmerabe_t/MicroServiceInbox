<?php
Doo::loadCore('db/DooModel');

class UsersBase extends DooModel{

    /**
     * @var int Max length is 50.
     */
    public $id;

    /**
     * @var varchar Max length is 50.
     */
    public $firstName;

    /**
     * @var varchar Max length is 50.
     */
    public $lastName;

    /**
     * @var varchar Max length is 50.
     */
    public $email;

    /**
     * @var date
     */
    public $created_date;

    public $_table = 'users';
    public $_primarykey = 'id';
    public $_fields = array('id','firstName','lastName','email','created_date');

    public function getVRules() {
        return array(
                'id' => array(
                        array( 'integer' ),
                        array( 'maxlength', 50 ),
                        array( 'optional' ),
                ),

                'firstName' => array(
                        array( 'maxlength', 50 ),
                        array( 'notnull' ),
                ),

                'lastName' => array(
                        array( 'maxlength', 50 ),
                        array( 'notnull' ),
                ),

                'email' => array(
                        array( 'maxlength', 50 ),
                        array( 'notnull' ),
                ),

                'created_date' => array(
                        array( 'date' ),
                        array( 'notnull' ),
                )
            );
    }

}