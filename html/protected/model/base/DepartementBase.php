<?php
Doo::loadCore('db/DooModel');

class DepartementBase extends DooModel{

    /**
     * @var int Max length is 11.
     */
    public $departement_id;

    /**
     * @var varchar Max length is 3.
     */
    public $departement_code;

    /**
     * @var int Max length is 5.
     */
    public $region_code;

    /**
     * @var varchar Max length is 255.
     */
    public $departement_nom;

    /**
     * @var varchar Max length is 255.
     */
    public $departement_nom_uppercase;

    /**
     * @var varchar Max length is 255.
     */
    public $departement_slug;

    /**
     * @var varchar Max length is 20.
     */
    public $departement_nom_soundex;

    public $_table = 'departement';
    public $_primarykey = 'departement_id';
    public $_fields = array('departement_id','departement_code','region_code','departement_nom','departement_nom_uppercase','departement_slug','departement_nom_soundex');

    public function getVRules() {
        return array(
                'departement_id' => array(
                        array( 'integer' ),
                        array( 'maxlength', 11 ),
                        array( 'optional' ),
                ),

                'departement_code' => array(
                        array( 'maxlength', 3 ),
                        array( 'optional' ),
                ),

                'region_code' => array(
                        array( 'integer' ),
                        array( 'maxlength', 5 ),
                        array( 'notnull' ),
                ),

                'departement_nom' => array(
                        array( 'maxlength', 255 ),
                        array( 'optional' ),
                ),

                'departement_nom_uppercase' => array(
                        array( 'maxlength', 255 ),
                        array( 'optional' ),
                ),

                'departement_slug' => array(
                        array( 'maxlength', 255 ),
                        array( 'optional' ),
                ),

                'departement_nom_soundex' => array(
                        array( 'maxlength', 20 ),
                        array( 'optional' ),
                )
            );
    }

}