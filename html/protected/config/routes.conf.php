<?php

$route['get']['/genModel'] = array(
		'BDDController',
		'genModel'
);

$route['get']['/genBDD'] = array(
		'BDDController',
		'genBDD'
);

/**
 * USERS
 */
// GET
$route['get']['/inbox/:match_id'] = array(
		'Ctrl/InboxCtrl',
		'getOneConversation'
);
$route['get']['/inbox'] = array(
		'Ctrl/InboxCtrl',
		'getAllConversation'
);

$route['get']['/inboxMock'] = array(
		'Ctrl/InboxCtrl',
		'inboxMock'
);

// POST
$route['post']['/inbox/:match_id'] = array(
		'Ctrl/InboxCtrl',
		'CreateOneMessage'
);

$route['post']['/inbox'] = array(
		'Ctrl/InboxCtrl',
		'CreateOneConversation'
);

?>