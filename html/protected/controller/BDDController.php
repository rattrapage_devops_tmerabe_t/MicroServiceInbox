<?php
include 'dooframework/db/DooModelGen.php';

class BDDController extends DooController {

	const ERR_SYS = 500;
	const ERR_DEV = 400;
	const CODE_SUCCESS = 202; 
	
	public function genModel() {
		DooModelGen::gen_mysql();
	}
	
	public function genBDD() {
		$mysqli = new mysqli('mysql_server', 'demo', 'demo', "db");

		$templine = '';
		$lines = file("init.sql");
		foreach ($lines as $line) {
			if (substr($line, 0, 2) == '--' || $line == '') {
				continue;
			}
			$templine .= $line;
			if (substr(trim($line), -1, 1) == ';') {
				mysqli_query($mysqli, $templine) or print('Error performing query \'<strong>' . $templine . '\': ' . $mysqli->errno . '<br /><br />');
				$templine = '';
			}
		}
		echo "Tables imported successfully";
	}
	
	public function renderJSON($contents = "Hello world!", $statusCode = self::CODE_SUCCESS) {
	    $this->setContentType('json');
	    http_response_code($statusCode);
	    echo "$contents";
	}
}