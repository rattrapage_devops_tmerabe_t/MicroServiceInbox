<?php
Doo::loadCore('db/DooModel');

class AnnoncesBase extends DooModel{

    /**
     * @var int Max length is 11.
     */
    public $idAnnonce;

    /**
     * @var int Max length is 11.
     */
    public $idProprio;

    /**
     * @var char Max length is 20.
     */
    public $type;

    /**
     * @var tinyint Max length is 2.
     */
    public $nbPieces;

    /**
     * @var decimal Max length is 18. ,8).
     */
    public $loyer;

    /**
     * @var char Max length is 20.
     */
    public $ville;

    /**
     * @var char Max length is 40.
     */
    public $adresse;

    /**
     * @var char Max length is 7.
     */
    public $codePostal;

    /**
     * @var char Max length is 100.
     */
    public $departement;

    /**
     * @var char Max length is 20.
     */
    public $region;

    /**
     * @var decimal Max length is 18. ,8).
     */
    public $lat;

    /**
     * @var decimal Max length is 18. ,8).
     */
    public $lng;

    /**
     * @var varchar Max length is 255.
     */
    public $description;

    public $_table = 'annonces';
    public $_primarykey = 'idAnnonce';
    public $_fields = array('idAnnonce','idProprio','type','nbPieces','loyer','ville','adresse','codePostal','departement','region','lat','lng','description');

    public function getVRules() {
        return array(
                'idAnnonce' => array(
                        array( 'integer' ),
                        array( 'maxlength', 11 ),
                        array( 'optional' ),
                ),

                'idProprio' => array(
                        array( 'integer' ),
                        array( 'maxlength', 11 ),
                        array( 'notnull' ),
                ),

                'type' => array(
                        array( 'maxlength', 20 ),
                        array( 'notnull' ),
                ),

                'nbPieces' => array(
                        array( 'integer' ),
                        array( 'maxlength', 2 ),
                        array( 'notnull' ),
                ),

                'loyer' => array(
                        array( 'float' ),
                        array( 'notnull' ),
                ),

                'ville' => array(
                        array( 'maxlength', 20 ),
                        array( 'notnull' ),
                ),

                'adresse' => array(
                        array( 'maxlength', 40 ),
                        array( 'notnull' ),
                ),

                'codePostal' => array(
                        array( 'maxlength', 7 ),
                        array( 'notnull' ),
                ),

                'departement' => array(
                        array( 'maxlength', 100 ),
                        array( 'notnull' ),
                ),

                'region' => array(
                        array( 'maxlength', 20 ),
                        array( 'notnull' ),
                ),

                'lat' => array(
                        array( 'float' ),
                        array( 'notnull' ),
                ),

                'lng' => array(
                        array( 'float' ),
                        array( 'notnull' ),
                ),

                'description' => array(
                        array( 'maxlength', 255 ),
                        array( 'notnull' ),
                )
            );
    }

}