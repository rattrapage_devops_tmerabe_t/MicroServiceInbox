<?php
Doo::loadCore('db/DooModel');

class PhotoAnnonceBase extends DooModel{

    /**
     * @var int Max length is 11.
     */
    public $idPhotoAnnonce;

    /**
     * @var int Max length is 11.
     */
    public $idPhoto;

    /**
     * @var int Max length is 11.
     */
    public $idAnnonce;

    public $_table = 'photo_annonce';
    public $_primarykey = 'idPhotoAnnonce';
    public $_fields = array('idPhotoAnnonce','idPhoto','idAnnonce');

    public function getVRules() {
        return array(
                'idPhotoAnnonce' => array(
                        array( 'integer' ),
                        array( 'maxlength', 11 ),
                        array( 'optional' ),
                ),

                'idPhoto' => array(
                        array( 'integer' ),
                        array( 'maxlength', 11 ),
                        array( 'notnull' ),
                ),

                'idAnnonce' => array(
                        array( 'integer' ),
                        array( 'maxlength', 11 ),
                        array( 'notnull' ),
                )
            );
    }

}