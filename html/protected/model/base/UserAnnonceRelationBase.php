<?php
Doo::loadCore('db/DooModel');

class UserAnnonceRelationBase extends DooModel{

    /**
     * @var int Max length is 11.
     */
    public $idUserAnnonce;

    /**
     * @var int Max length is 11.
     */
    public $idUser;

    /**
     * @var int Max length is 11.
     */
    public $idAnnonce;

    public $_table = 'user_annonce_relation';
    public $_primarykey = 'idUserAnnonce';
    public $_fields = array('idUserAnnonce','idUser','idAnnonce');

    public function getVRules() {
        return array(
                'idUserAnnonce' => array(
                        array( 'integer' ),
                        array( 'maxlength', 11 ),
                        array( 'optional' ),
                ),

                'idUser' => array(
                        array( 'integer' ),
                        array( 'maxlength', 11 ),
                        array( 'notnull' ),
                ),

                'idAnnonce' => array(
                        array( 'integer' ),
                        array( 'maxlength', 11 ),
                        array( 'notnull' ),
                )
            );
    }

}