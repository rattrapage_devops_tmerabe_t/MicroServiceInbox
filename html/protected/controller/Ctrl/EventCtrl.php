<?php

Doo::loadModel('Events');
Doo::loadController('BDDController');

class EventCtrl extends BDDController {
	
	public function getEventsByUserId() {
		$options = array('select' => '*', 'AsArray' => 'true', 'where' => "idUser = '" . $this->params['idUser'] . "'");
		$events = Events::_find("Events", $options);
		return $this->renderJSON(json_encode($events));
	}
	
	public function saveEvent() {
		$idUser = $this->params['idUser'];
		$data = file_get_contents("php://input");
	    $data = json_decode($data);
		$event = new Events($data);
		$event->idUser = $idUser;
		if (isset($event->idEvent) && $event->idEvent > 0) {
			return $this->renderJSON(json_encode($event->update()));
		}
		return $this->renderJSON(json_encode($event->insert()));
	}
	
	public function deleteEvent() {
		$options = array('where' => "idEvent = '" . $this->params['idEvent'] . "'");
		Events::_delete("Events", $options);
		return $this->renderJSON(json_encode("Evenement Supprimé"));
	}
}