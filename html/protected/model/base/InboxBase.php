<?php
Doo::loadCore('db/DooModel');

class InboxBase extends DooModel{

    /**
     * @var int Max length is 50.
     */
    public $id;

    /**
     * @var int Max length is 50.
     */
    public $from;

    /**
     * @var int Max length is 50.
     */
    public $to;

    /**
     * @var int Max length is 50.
     */
    public $match_id;

    /**
     * @var date
     */
    public $sent_date;

    /**
     * @var date
     */
    public $created_date;

    /**
     * @var varchar Max length is 1000.
     */
    public $content;

    public $_table = 'inbox';
    public $_primarykey = '';
    public $_fields = array('id','from','to','match_id','sent_date','created_date','content');

    public function getVRules() {
        return array(
                'id' => array(
                        array( 'integer' ),
                        array( 'maxlength', 50 ),
                        array( 'notnull' ),
                ),

                'from' => array(
                        array( 'integer' ),
                        array( 'maxlength', 50 ),
                        array( 'notnull' ),
                ),

                'to' => array(
                        array( 'integer' ),
                        array( 'maxlength', 50 ),
                        array( 'notnull' ),
                ),

                'match_id' => array(
                        array( 'integer' ),
                        array( 'maxlength', 50 ),
                        array( 'notnull' ),
                ),

                'sent_date' => array(
                        array( 'date' ),
                        array( 'notnull' ),
                ),

                'created_date' => array(
                        array( 'date' ),
                        array( 'notnull' ),
                ),

                'content' => array(
                        array( 'maxlength', 1000 ),
                        array( 'notnull' ),
                )
            );
    }

}