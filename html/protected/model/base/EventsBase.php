<?php
Doo::loadCore('db/DooModel');

class EventsBase extends DooModel{

    /**
     * @var int Max length is 11.
     */
    public $idEvent;

    /**
     * @var int Max length is 11.
     */
    public $idUser;

    /**
     * @var varchar Max length is 200.
     */
    public $title;

    /**
     * @var timestamp
     */
    public $startTime;

    /**
     * @var timestamp
     */
    public $endTime;

    /**
     * @var tinyint Max length is 1.
     */
    public $allDay;

    public $_table = 'events';
    public $_primarykey = 'idEvent';
    public $_fields = array('idEvent','idUser','title','startTime','endTime','allDay');

    public function getVRules() {
        return array(
                'idEvent' => array(
                        array( 'integer' ),
                        array( 'maxlength', 11 ),
                        array( 'optional' ),
                ),

                'idUser' => array(
                        array( 'integer' ),
                        array( 'maxlength', 11 ),
                        array( 'notnull' ),
                ),

                'title' => array(
                        array( 'maxlength', 200 ),
                        array( 'notnull' ),
                ),

                'startTime' => array(
                        array( 'datetime' ),
                        array( 'optional' ),
                ),

                'endTime' => array(
                        array( 'datetime' ),
                        array( 'notnull' ),
                ),

                'allDay' => array(
                        array( 'integer' ),
                        array( 'maxlength', 1 ),
                        array( 'notnull' ),
                )
            );
    }

}