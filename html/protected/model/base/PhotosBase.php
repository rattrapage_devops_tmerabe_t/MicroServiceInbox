<?php
Doo::loadCore('db/DooModel');

class PhotosBase extends DooModel{

    /**
     * @var int Max length is 11.
     */
    public $idPhoto;

    /**
     * @var char Max length is 200.
     */
    public $lien;

    public $_table = 'photos';
    public $_primarykey = 'idPhoto';
    public $_fields = array('idPhoto','lien');

    public function getVRules() {
        return array(
                'idPhoto' => array(
                        array( 'integer' ),
                        array( 'maxlength', 11 ),
                        array( 'optional' ),
                ),

                'lien' => array(
                        array( 'maxlength', 200 ),
                        array( 'notnull' ),
                )
            );
    }

}