<?php

Doo::loadModel('UserAnnonceRelation');
Doo::loadModel('Users');
Doo::loadModel('DocumentConsultationPermit');
Doo::loadModel('UserDocuments');
Doo::loadController('BDDController');
Doo::loadModel('Annonces');

class DocumentsCtrl extends BDDController {

	/**
	 * GET ALLOWED DOCUMENTS
	 * FOR GETTING DOCS OF LOCATAIRES CARRING OF OUR ANNOUNCES
	 *
	 * @return unknown
	 */
	public function getAllowedDocuments() {
		
		// 1) get owned annonces
		$idProprio = $this->params['idUser'];
		$a = new Annonces();
		$opt = array(
				'where' => 'idProprio = ?',
				'param' => array(
						$this->params['idUser']
				)
		);
		$annonces = $a->find($opt, $this->params['idUser']);
		
		$annonces = $this->getUserAndDocsAllowed($annonces);
		
		return $this->renderJSON(json_encode($annonces));
	}

	public function getUserAndDocsAllowed($annonces) {
		// 2) get user and docs allowed for this annonce.
		if (empty($annonces)) {
			return null;
		}
		foreach ($annonces as $annonce) {
			// 3) get users allowed
			$idAnnonce = $annonce->idAnnonce;
			$dcp = new DocumentConsultationPermit();
			$opt = array(
					'where' => 'idAnnonce = ?',
					'param' => array(
							$idAnnonce
					)
			);
			$dcps = $dcp->find($opt);
			
			$users = [];
			// 4) for all users, get user info and docs public
			foreach ($dcps as $dcp) {
				$idUser = $dcp->idUser;
				$users[] = $this->getUserInfos($idUser);
			}
			$annonce->usersAllowDoc = $users;
		}
		return $annonces;
	}

	public function getUserInfos($idUser) {

		$u = new Users();
		$options = array(
				'where' => "idUser = '$idUser'"
		);
		$users = $u->find($options);
		$user = array_pop($users);
		if ($user == null) {
			return null;
		} else {
			$ud = new UserDocuments();
			$options = array(
					'where' => "idUser = '$idUser'"
			);
			$docs = $ud->find($options);
			$user->docs = $docs;
			return $user;
		}
	}

	/**
	 * END getAllowedDocuments
	 *
	 * @return unknown
	 */
	
	public function getMyDocuments() {

		$doc = new UserDocuments();
		$options = array(
				"where" => "idUser = ?",
				"param" => array(
						$this->params['idUser']
				)
		);
		$docs = $doc->find($options);
		return $this->renderJSON(json_encode($docs));
	}

	public function getUserDocumentById() {

		$doc = new UserDocuments();
		$options = array(
				"where" => "idDocument = ?",
				"param" => array(
						$this->params['idDocument']
				)
		);
		$docs = $doc->find($options);
		return $this->renderJSON(json_encode(array_pop($docs)));
	}

	public function saveDocumentConsultationPermit() {

		$data = json_decode(file_get_contents("php://input"));
		$dcp = new DocumentConsultationPermit($data);
		return $this->renderJSON(json_encode($dcp->insert()));
	}

	public function uploadDocumentLocataire() {

		$data = json_decode(file_get_contents("php://input"));
		$param = $this->params['idUser'];
		$path = "upload/documents/user_" . $param;
		// $devTestPath = "C:\\xampp\\htdocs\\ink_web_server\\protected\\controller\\Ctrl\\upload\\documents\\user_" . $param;
		
		$doc = $data->doc;
		// Remplacement de la photo
		if (isset($data->img)) {
			if (isset($devTestPath)) {
				$path = $devTestPath;
			}
			$date = date('l_jS_F_Y_H\hi');
			$img = $data->img;
			$uploadFolder = $path . "_" . $date . $data->doc->type;
			$img = base64_decode(preg_replace('/^[^,]*,/', '', $img));
			$result = file_put_contents($uploadFolder, $img);
		}
		
		if (isset($result) && ! $result) {
			return $this->renderJSON(json_encode($result), self::ERR_SYS);
		} else {
			$userDoc = new UserDocuments($data->doc);
			if (isset($this->params['idDocument']) && $this->params['idDocument'] != 0 && $img) {
				array_map('unlink', glob($userDoc->link));
				$userDoc->link = $uploadFolder;
			}
			$userDoc->idUser = $param;
			if ($userDoc->idDocument == 0) {
				$userDoc->insert();
			} else {
				$userDoc->update();
			}
			return $this->renderJSON(json_encode("Photo modifiee"));
		}
	}

	public function delDocumentConsultationPermit() {

		$del = DocumentConsultationPermit::_delete('DocumentConsultationPermit', array(
				'where' => 'idAnnonce = ? AND idUser = ?',
				'param' => array(
						$this->params['idAnnonce'],
						$this->params['idUser']
				)
		));
		return $this->renderJSON(json_encode($del));
	}

	public function delDocument() {

		$del = UserDocuments::_delete('UserDocuments', array(
				'where' => 'idDocument = ?',
				'param' => array(
						$this->params['idDocument']
				)
		));
		return $this->renderJSON(json_encode($del));
	}

}