<?php
Doo::loadCore('db/DooModel');

class MessageBase extends DooModel{

    /**
     * @var int Max length is 11.
     */
    public $idMessage;

    /**
     * @var int Max length is 11.
     */
    public $idFrom;

    /**
     * @var int Max length is 11.
     */
    public $idTo;

    /**
     * @var char Max length is 2.
     */
    public $unread;

    /**
     * @var datetime
     */
    public $sendDate;

    /**
     * @var date
     */
    public $readDate;

    /**
     * @var text
     */
    public $content;

    public $_table = 'message';
    public $_primarykey = 'idMessage';
    public $_fields = array('idMessage','idFrom','idTo','unread','sendDate','readDate','content');

    public function getVRules() {
        return array(
                'idMessage' => array(
                        array( 'integer' ),
                        array( 'maxlength', 11 ),
                        array( 'optional' ),
                ),

                'idFrom' => array(
                        array( 'integer' ),
                        array( 'maxlength', 11 ),
                        array( 'notnull' ),
                ),

                'idTo' => array(
                        array( 'integer' ),
                        array( 'maxlength', 11 ),
                        array( 'notnull' ),
                ),

                'unread' => array(
                        array( 'maxlength', 2 ),
                        array( 'notnull' ),
                ),

                'sendDate' => array(
                        array( 'datetime' ),
                        array( 'notnull' ),
                ),

                'readDate' => array(
                        array( 'date' ),
                        array( 'notnull' ),
                ),

                'content' => array(
                        array( 'notnull' ),
                )
            );
    }

}